#!/bin/bash -eux

export DEBIAN_FRONTEND=noninteractive

######################################
# install STS
######################################
mkdir -p /tmp/sts
cd /tmp/sts

wget -q https://download.springsource.com/release/STS/3.9.7.RELEASE/dist/e4.10/spring-tool-suite-3.9.7.RELEASE-e4.10.0-linux-gtk-x86_64.tar.gz

mkdir -p /opt/sts
tar -xf spring-tool-suite-3.9.7.RELEASE-e4.10.0-linux-gtk-x86_64.tar.gz -C /opt/sts

echo "[Desktop Entry]
Type=Application
Name=Spring Tool Suite (STS)
Comment=Spring Tool Suite
Icon=/opt/sts/sts-bundle/sts-3.9.7.RELEASE/icon.xpm
Exec=/opt/sts/sts-bundle/sts-3.9.7.RELEASE/STS
Terminal=false
Categories=Development;IDE;Java;
StartupWMClass=STS" > /usr/share/applications/sts.desktop

# install openJDK 8
apt-get update
apt-get install -y -qq openjdk-8-jdk

######################################
# install IntelliJ
######################################

mkdir /tmp/int
cd /tmp/int
wget -q -O int.tar.gz "https://download.jetbrains.com/product?code=IIU&latest&distribution=linux" 

#Install to the /opt directory. Should create /opt/ideaIU
mkdir -p /opt/ideaIU
tar -zxf ./int.tar.gz -C /opt/ideaIU --strip-components=1

# Set inotify large enough for projects with thousands of files. Required by IntelliJ
echo "fs.inotify.max_user_watches = 524288" > /etc/sysctl.d/idea.conf
sysctl -p --system

######################################
# install InetelliJ plugins
######################################
cd /opt/ideaIU/plugins

# Terraform
wget -q -O tf.zip https://plugins.jetbrains.com/files/7808/70227/intellij-terraform-0.7.7.zip
unzip -qq -o tf.zip

# Python

wget -q -O python.zip https://plugins.jetbrains.com/files/631/73843/python.zip
unzip -qq -o python.zip

# Rainbow Brackets
wget -q -O rainbow.zip https://plugins.jetbrains.com/files/10080/73032/intellij-rainbow-brackets-5.27.zip
unzip -qq -o rainbow.zip

# Save Actions
wget -q -O save-actions.jar https://plugins.jetbrains.com/files/7642/72931/intellij-plugin-save-actions-1.7.0+2019.3.jar

# Gauge
wget -q -O gauge.zip https://plugins.jetbrains.com/files/7535/73675/Gauge-Java-Intellij-0.3.18.zip
unzip -qq -o gauge.zip


echo "[Desktop Entry]
Type=Application
Name=IntelliJ Idea
Comment=IntelliJ
Icon=/opt/ideaIU/bin/idea.svg
Exec=/opt/ideaIU/bin/idea.sh
Terminal=false
Categories=Development;IDE;Java;" > /usr/share/applications/intellij.desktop

#Cleanup the temp directory
rm -rf /tmp/int

######################################
# install DBeaver
######################################

mkdir /tmp/dbeaver
cd /tmp/dbeaver

wget -q https://dbeaver.io/files/dbeaver-ce_latest_amd64.deb
apt install -qq -y ./dbeaver-ce_latest_amd64.deb

echo "[Desktop Entry]
Comment=
Terminal=False
Name=DBeaver
Icon=/usr/share/dbeaver/dbeaver.png
Exec=dbeaver
Type=Application
Name[en_US]=DBeaver" > /usr/share/applications/dbeaver.desktop

rm -rf /tmp/dbeaver
