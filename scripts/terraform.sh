#!/bin/bash -eux
export DEBIAN_FRONTEND=noninteractive

apt-get install unzip
mkdir -p /tmp/terraform
cd /tmp/terraform

wget -q https://releases.hashicorp.com/terraform/0.11.13/terraform_0.11.13_linux_amd64.zip
unzip -qq -o terraform_0.11.13_linux_amd64.zip
mv terraform /usr/local/bin/

# Add sumologic plugin
mkdir -p /home/vagrant/.terraform.d/plugins
wget -q https://github.com/SumoLogic/sumologic-terraform-provider/releases/download/v0.3.0/sumologic-terraform-provider_0.3.0_Linux_64-bit.zip
unzip -qq -o ./sumologic-terraform-provider_0.3.0_Linux_64-bit.zip


wget -q https://releases.hashicorp.com/terraform/0.12.8/terraform_0.12.8_linux_amd64.zip
unzip -qq -o terraform_0.12.8_linux_amd64.zip
mv terraform /usr/local/bin/terraform12
